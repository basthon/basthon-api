# Basthon-API

This repository implements the Basthon's remote API on a Node.js web server.

It is mainly used to implements the support of Kernel Threads Synchronous Communication
in environments that do not support SharedArrayBuffer.
