import { IncomingMessage, ServerResponse } from "http";
import { RequestMap } from "./request-map";

interface PostBody {
  action: "get" | "post";
  key: string;
  payload?: string;
}

class EphemeralHandler {
  private _maxPosts: number;
  private _maxSize: number;
  private _ttl: number;
  private _contents = new RequestMap<string>();

  constructor(maxPosts: number, maxSize: number, ttl: number) {
    this._maxPosts = maxPosts;
    this._maxSize = maxSize;
    this._ttl = ttl;
  }

  /**
   * Handle a request on the /ephemeral API point.
   */
  public async handle(
    request: IncomingMessage,
    response: ServerResponse,
  ): Promise<void> {
    try {
      await this._handle(request, response);
    } catch (e) {
      let status = 400;
      if (typeof e === "number") status = e;
      response.statusCode = status;
      response.end();
    }
  }

  /**
   * Handle request throwing status code number on errors.
   */
  private async _handle(
    request: IncomingMessage,
    response: ServerResponse,
  ): Promise<void> {
    // check method
    if (request.method !== "POST") throw 405; // method not allowed

    // get content-length
    let length: number | undefined;
    try {
      length = parseInt(request.headers["Content-Length"] as string);
    } catch (e) {
      throw 411; // length required
    }
    if (length > this._maxSize) throw 413; // payload too large

    // get body
    let data: PostBody | undefined;
    try {
      data = await this.readData<PostBody>(request);
    } catch (e) {
      throw 400; // bad request
    }

    // answer
    const key = data?.key;
    if (key == null) throw 400; // bad request
    await this._contents.clearOlds(this._ttl);
    const payload = data?.payload;
    switch (data?.action) {
      case "get":
        let content: undefined | string;
        try {
          content = await this._contents.get(key, {
            removeKey: true,
            timeout: 16,
          });
        } catch (e) {
          throw 400; // bad request
        }
        response.setHeader("Content-Type", "text/plain");
        response.end(content);
        return;
      case "post":
        if (payload == null) throw 400; // bad request
        if (this._contents.size >= this._maxPosts - 1) throw 429; // too many requests
        await this._contents.set(key, payload);
        response.end();
        return;
      default:
        throw 400; // bad request
    }
  }

  /**
   * Read data from input request and return an object of generic type.
   */
  private async readData<T>(request: IncomingMessage): Promise<T> {
    return new Promise((resolve, reject) => {
      const chunks: Buffer[] = [];
      request.on("data", (chunk: Buffer) => chunks.push(chunk));
      request.on("end", () => {
        try {
          resolve(JSON.parse(Buffer.concat(chunks).toString()));
        } catch (e) {
          reject();
        }
      });
    });
  }
}

export { EphemeralHandler };
