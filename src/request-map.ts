/**
 * A promise that reject after a given timeout if not resolved.
 */
class TimeoutPromise<T> {
  private _promise: Promise<T>;
  private _resolve?: (_: T) => void;
  private _reject?: () => void;

  constructor(timeout: number) {
    this._promise = new Promise<T>((resolve, reject) => {
      const timeoutID = globalThis.setTimeout(reject, timeout);
      this._resolve = (t: T) => {
        globalThis.clearTimeout(timeoutID);
        resolve(t);
      };
      this._reject = (): void => {
        globalThis.clearTimeout(timeoutID);
        reject();
      };
    });
  }

  public get promise(): Promise<T> {
    return this._promise;
  }

  public get resolve() {
    return this._resolve!;
  }

  public get reject() {
    return this._reject!;
  }
}

/**
 * A class to manage contents that can be added and consumed by network requests.
 * The get method is able to wait for a key/content to be added.
 * This is the key point to avoid client ping loop.
 */
export class RequestMap<T> {
  private _contents = new Map<string, { timestamp: number; content: T }>();
  private _waintingRequests = new Map<string, (_: T) => void>();

  constructor() {}

  public get size(): number {
    return this._contents.size;
  }

  /**
   * Get data from the map, wait if not present.
   *
   * Options:
   *   timeout: default timeout is 16 seconds.
   *   removeKey: should we remove the key after we got it
   */
  public async get(
    key: string,
    options?: { removeKey?: boolean; timeout?: number },
  ): Promise<T> {
    if (this._contents.has(key)) {
      const res = this._contents.get(key)!.content;
      if (options?.removeKey === true) this._contents.delete(key);
      return res;
    } else {
      const timeout = options?.timeout ?? 16;
      const promise = new TimeoutPromise<T>(1000 * timeout);
      this._waintingRequests.set(key, promise.resolve);
      let content: undefined | T;
      try {
        content = await promise.promise;
      } catch (e) {
        // timeout! so resolver should be removed
        this._waintingRequests.delete(key);
        throw e;
      }
      // we store content if not asked to be removed
      if (options?.removeKey !== true)
        this._contents.set(key, { timestamp: Date.now(), content });
      return content;
    }
  }

  /**
   * Add data to the map. Resolve pending request if any.
   */
  public async set(key: string, content: T): Promise<void> {
    if (this._waintingRequests.has(key))
      this._waintingRequests.get(key)!(content);
    else this._contents.set(key, { timestamp: Date.now(), content });
  }

  /**
   * Remove contents older than TTL.
   */
  public async clearOlds(ttl: number): Promise<void> {
    const tsMin = Date.now() - ttl;
    for (const [key, { timestamp }] of this._contents)
      if (timestamp < tsMin) this._contents.delete(key);
  }
}
