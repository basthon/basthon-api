import { createServer, IncomingMessage, ServerResponse } from "http";
import { EphemeralHandler } from "./ephemeral";

const port: number = 5000;
// 1 million posts, 1KB max per post, 2 minutes
const ephemeral = new EphemeralHandler(1e6, 1024, 2 * 60 * 1000);

const server = createServer(
  async (request: IncomingMessage, response: ServerResponse) => {
    response.setHeader("Cache-Control", "must-revalidate, no-cache, private");
    response.setHeader("Access-Control-Allow-Origin", "*");

    // reject http request
    if (request.headers["x-forwarded-proto"] === "http") {
      response.statusCode = 400;
      response.end();
      return;
    }

    // accept options requests
    if (request.method === "OPTIONS") {
      response.setHeader("Access-Control-Allow-Headers", "X-API-Referer");
      response.end();
    }

    // reject request without referer
    const referer = request.headers["x-api-referer"];
    if (referer == null || referer === "null") {
      response.statusCode = 400;
      response.end();
      return;
    }

    // handle other requests
    switch (request.url) {
      case "/ephemeral":
        await ephemeral.handle(request, response);
        break;
      default:
        response.statusCode = 404;
        response.end();
        break;
    }
    // request handled
  }
);

server.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
